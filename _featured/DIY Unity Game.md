--- 
image: /images/blog/rimjourney.png
layout: post
title: DIY Unity Game
---


![Rim's Journey Logo](/images/blog/rimjourney.png)

  
  
## Introduction
Hello, my name is Harsh Goswami. I made the game called Rim’s Journey. In this blog I will talk about:
- How I made it
- Some of the problems I faced
- Tips about game development. 

This was my first game that I was going to publish, so naturally I was pretty nervous. I didn't expect a lot of players thinking,  "you won't get many players, this is like your first game and not a lot of people know about you and its your game." 

## The Story
I started learning about game development when I was 14. I continued to learn and I just kinda taught myself how everything works, so after a year when I turned 15 I thought I would start making a game. Just a small game nothing too big in order to get a project out there and know the feeling of releasing a game and gaining a small audience if you can. I used Unity at that time to make this game. For beginners I would recommend Unity. When I first started working on this, I had a million ideas for this game. However, I forgot that this is just my first game, and I needed something really small. This is another thing you should remember, if it's your first game, make something really small and understand what it means to release a game. To be honest with myself, this game was a failure but at the exact same time it wasn't. I didn't put all the effort I should have but the good thing was that I understand what and where I went wrong. I did learn from my mistakes. It doesn't matter how many mistakes you make as long as you learn from them.

[Play the Game!](https://harsh-goswmai.itch.io/rims-journey)