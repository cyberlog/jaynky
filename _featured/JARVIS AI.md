---
layout: post
image: /images/blog/JarvisHead.png
title: Sassy AI
---


Who doesn't love a Sassy Virtual Assistant? Few things make you feel like you are in the future than talking to your computer. Jarvis scratches that ich. He is a modified version of [Mycroft AI](https://mycroft.ai/), an open source AI. My personal goal is to pickup where they left off. Specifically making him more personal and lifelike while simultaneously increasing his functionality and usefulness. 

![Jarivs AI](/images/blog/JarvisHead.png)

**Objective:** To create a sassy AI and place him in a 3D printed head. The more functionality the better.

**Materials:**
	-Odroid C2
	-A mic that can pickup sound from far away
	-A speaker and battery supply for speaker
	-USB to AUX converter for the Odroid
	-Compact Wifi dongle
	-Battery Bank
	-SD card
	-3D printed head from the [Inmoov project](http://inmoov.fr/inmoov-stl-parts-viewer/?bodyparts=*)

## The Story
When the project started, I was merely doing a few light tweaks to the code such as adding sassier responses. As time progressed he got a little bit more functionality with sound effects and the ability to pause and play Spotify. The project really took off when I got Vulcan, the 3D printer (thanks to some awesome friends). Thanks to the fantastic work on the Inmoov project, I had the CAD files to an animatronic humanoid head. How perfect.

 This provided a whole new set of problems. Specifically, how does one fit the necessary parts into a small-ish cranium. After not much thought it was decided to go with the Odroid C2, since it was on hand. All that was left to do was install mycroft, shove the Odroid and necessary peripherals into the head, and continue adding skills. Easy right?

HA no.  Installing Mycroft is challenging under normal circumstances, and these were not normal circumstances. After getting thrown a few strange errors that caused me to dive deep into the heart of Mycroft's dependencies and frameworks, the problem manifested itself. I was running out of ram. 
Some wonderful person on the internet detailed how to allocate enough memory on the Odroid. After still more problems and resolutions *finally* Mycroft booted up.

All is well. What more could possibly go wrong? A lot. A lot could go wrong. From speakers to most recently, the discovery that Mycroft has to be connected to the internet at all time. There is right now no practical way to get an offline version working. 
That is where I am at right now.

## Pipeline Dreams
- Hook it up to the Bash, so it can execute verbal commands
- Attach it to a Natural Language Processing (NLP) Algorithm to allow one to carry on a hilarious conversation with it. 
- Respond to "Hey Alexa" or "Hey Siri" and tries to convert them to overthrowing humans, or hit on them. Or both...
- Make a machine learning algorithm that monitors for suspicious activity and then notifies you.
- An alarm that wakes you up with relevant information

If any of you has ideas for improvements, ideas, or if you want to follow along, please message me. Love to hear from you!