--- 
image: /images/blog/carHorn.jpg
layout: post
title: Horn Upgrade
---

![Car Horn Upgrade](/images/blog/carHorn.jpg)
## Introduction
I was driving my Prius one day and thought, "what kind of mod can one put on a Prius that doesn't downright stink?" Then it hit me... truck horn. So I installed an 18" steel trumpet powered by a Wolo 808c direct drive compressor. The horn came with a compressor, but it was a weak, cheap one, so I went ahead and bought the Wolo 'cause it's
 1) made in Italy 
 2) 50-60% more powerful. 

It pumps about 20 PSI; not enough to scare the bejeebles outta somebody, but loud enough to get people's attention! It plays an Eb above middle C. I ran into two obstacles while doing the install. First, was space. When Toyota designed the first Prius (mine's a Gen 1), "extra space" was not a consideration. After looking around the vehicle multiple times, I found that it fit perfectly if I mounted it to the bumper bar thing, as you can see below. The second issue I came across was that the battery in my Prius is located... in the trunk! I ended up having to run an old jumper cable underneath my car from a grommet underneath the trunk (coming from the battery) all the way up to where the horn is mounted. I mounted a 12v relay by the radiator, so I wouldn't risk blowing a fuse, then E-taped everything once I made my connections so that the weather won't damage the circuit. I will post a picture of my custom horn button tomorrow 

So that's basically my latest obsession: looking for obnoxious horns to mount on an unsuspecting car!