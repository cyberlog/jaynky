---
layout: post
title: Jtheme
description: A high performing Jekyll theme that is streamlined for web development.
image: /images/jtheme.png
gitlab: https://gitlab.com/jaynky/jtheme
---

<img src="/images/jtheme.png">

In order to speed up the development process, we created a highly optimized template. Everything from 
page loading speeds to accessibility was finely tuned to speed up the development process. 


