---
layout: post
title: Bartlett Enterprises
image: /images/bartlett.png
description: Bartlett Enterprises is a medium sized machine shop that specializes in high-preciion machining
link: http://bartlett-enterprises.com/
gitlab: https://gitlab.com/jaynky/bartlett-webpage
---


<a href=""><img src="/images/bartlett.png" class="col-md-12">

<a href="http://bartlett-enterprises.com/">Bartlett Enterprises</a> is a family owned
machine shop that makes high-precision parts. This was one of our first professional websites. 
We used bootstrap to build and design the front-end of the site. Furthermore, we repositioned
Bartlett Enterprises so that it appeared higher in more relevant search results.