# Jaynky.com Design Doc
A quick overview of the Jaynky.com site

## Goals

- An archive for projects to help bolster community member's reputation and portfolio through making their projects easier to share.
- Create on online presence to grow the community people
- Informational hub to update people on competitions/events.
- Keep track of Badges/achievements for individuals

## Functionality

### Project Updates
A section or page will contain a stream from the project-updates channel. Only posts with pictures will be created by a bot (currently Nullbot). The post will then contain the text that the server member wrote.

### Featured Projects
A more curated section that will be written out instead of being collected and reposted by a bot. The featured projects will contain the best that the community has to offer. The featured projects will be more like blog posts than individual messages. 

### Badges 

A page that will rank users based off how close they are to achieving a certain badge. Details TBD.

### Friends of Jaynky.com	
Servers that are made by members of the community that we support. 

### Blog
The blog will contain important updates in the community as well as possibly the following item.

### Creators Hub (name TBD)
Other smaller projects that people have created (games, websites, apps, etc). Either this is a sub category of the above blog or its own entity.


## UI

The desired theme is best described as future drafting table.
A combination of graph paper and Cyan.



